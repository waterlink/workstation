#!/bin/bash

. config/defaults

username=`whoami`

sudo cp sudoer.conf /etc/sudoers.d/${username}
sudo chmod 0400 /etc/sudoers.d/${username}

# packages
sudo apt-get update -qq
sudo apt-get upgrade -yy -qq
sudo apt-get install -yy -qq -f openssh-server vim htop chromium-browser vagrant synapse zsh curl libcurl3 libcurl3-dev php5-curl php5-mcrypt



# cooker
sudo apt-get install -yy -qq -f ruby1.9.1-full
sudo gem1.9.1 install compass --no-ri --no-rdoc
./install_coffee.sh

# sublime
wget -c "$sublime_url" -O /tmp/sublime.deb
sudo dpkg -i /tmp/sublime.deb

# chrome
wget -c "$chrome_url" -O /tmp/chrome.deb
sudo dpkg -i /tmp/chrome.deb

[[ -f ~/.ssh/id_rsa ]] || ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa

[[ "${username}" = "user" ]] || sudo mkdir ln -s /home/${username} /home/user

mkdir -p ~/projects
./projects.sh

vagrant ssh -c 'hostname -I' | awk '{print $2}' | (read target_ip; nc -z $target_ip 22 || vagrant up; sudo ./hosts.sh $target_ip)

# oh my zsh
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh > /tmp/oh-my-zsh.sh
chmod +x /tmp/oh-my-zsh.sh
/tmp/oh-my-zsh.sh

chmod -R 755 /usr/local/share/zsh/site-functions

