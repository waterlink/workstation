#!/bin/bash

# add_one_host(ip, hostname)
function add_one_host {
  ip="$1"
  hostname="$2"
  echo "adding ${hostname} - ${ip}"
  grep -F "${ip}    ${hostname}" /etc/hosts || (echo "${ip}    ${hostname}" >> /etc/hosts)
}

target_host="$(hostname)-precise32"
target_ip=$1

echo "binding .lan's to ${target_ip}"

if nc -z "${target_ip}" 22; then
  echo "can bind"
  add_one_host "${target_ip}" "smart.lan"
  add_one_host "${target_ip}" "my.smart.lan"
  add_one_host "${target_ip}" "www.smart.lan"
  add_one_host "${target_ip}" "auth.smart.lan"
  add_one_host "${target_ip}" "admin.smart.lan"
else
  echo "cant bind"
fi


