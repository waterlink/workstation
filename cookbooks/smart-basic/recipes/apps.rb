
template '/etc/nginx/sites-enabled/smart.conf' do
  source 'smart.conf'
end

template '/etc/nginx/sites-enabled/auth-smart.conf' do
  source 'auth-smart.conf'
end

template '/etc/nginx/sites-enabled/admin-smart.conf' do
  source 'admin-smart.conf'
end


link '/home/vagrant/projects' do
  to '/home/user/projects'
end

