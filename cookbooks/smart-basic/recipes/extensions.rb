
include_recipe 'apt'

package 'php-pear'
package 'php5-dev'
package 'php5-curl'
package 'php5-mcrypt'
package 'build-essential'
package 'libpcre3-dev'
package 'libyaml-dev'

execute 'install pecl mongo extension' do
  command 'pecl install mongo-1.3.2 && echo "extension=mongo.so" > /etc/php5/conf.d/mongo.ini'
  not_if 'test -f /etc/php5/conf.d/mongo.ini'
end

execute 'install pecl apc extension' do
  command 'pecl install apc && echo "extension=apc.so" > /etc/php5/conf.d/apc.ini'
  not_if 'test -f /etc/php5/conf.d/apc.ini'
end

execute 'install pecl yaml extension' do
  command 'pecl install yaml && echo "extension=yaml.so" > /etc/php5/conf.d/yaml.ini'
  not_if 'test -f /etc/php5/conf.d/yaml.ini'
end

execute 'restart php-fpm' do
  command 'service php5-fpm restart'
end

