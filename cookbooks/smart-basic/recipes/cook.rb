
gem_package 'compass'


include_recipe 'nodejs::install_from_binary'
include_recipe 'nodejs::npm'

execute 'install coffeescript' do
  command 'npm install -g coffee-script'
end


execute 'cook assets' do
  command './cook.sh'
  cwd '/home/user/projects/smart'
  ignore_failure true
end

