
include_recipe 'apt'

package 'openssl'

apt_repository 'nginx-stable' do
  uri 'http://ppa.launchpad.net/nginx/stable/ubuntu'
  distribution node['lsb']['codename']
  components ["main"]
  keyserver "keyserver.ubuntu.com"
  key "C300EE8C"
end

template '/etc/hosts' do
  source 'hosts.erb'
  mode '0644'
end

include_recipe 'nginx'
include_recipe 'smart-basic::nginx'
include_recipe 'php-fpm'
include_recipe 'mongodb::10gen_repo'

include_recipe 'smart-basic::extensions'

include_recipe 'smart-basic::ssl'
include_recipe 'smart-basic::apps'

envjson = '{"environment": "development"}'

execute 'just development environment' do
  command "echo '#{envjson}' > /home/user/projects/environment.json"
end

directory '/var/scs'

link '/var/scs/environment.json' do
  to '/home/user/projects/environment.json'
end

include_recipe 'smart-basic::db'
#include_recipe 'smart-basic::cook'

execute 'start nginx' do
  command 'nginx -s reload || nginx'
end

