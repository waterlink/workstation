
cons = {
  :main => "rs_scsrsdep/dep-admin-mongo:27017,dep-mongo1:27017,dep-mongo2:27017",
  :auth => "dep-auth-mongo:27017"
}

dbs = {
  :scs_main => :main,
  :scs_users => :auth
}

execute 'rm db cache' do
  command 'rm -rf /tmp/db; mkdir -p /tmp/db'
  ignore_failure true
end

dbs.each do |db, dbcon|
  execute "dump db #{db} locally" do
    command "mongodump -h #{cons[dbcon]} -o /tmp/db/ -d #{db}; true"
    ignore_failure true
  end
end

execute "restore db locally" do
  command "mongorestore /tmp/db --drop; true"
  ignore_failure true
end


