
directory '/etc/nginx/ssl'

template '/etc/nginx/ssl/server.crt' do
  source 'server.crt'
end

template '/etc/nginx/ssl/server.key' do
  source 'server.key'
end

template '/etc/nginx/conf.d/ssl.conf' do
  source 'ssl.conf'
end

