#!/bin/bash

git=gituser
host=smart
projects=("smart -b test" "auth-smart -b refbs" "admin-smart -b refbs")


echo -n "Please add my ssh key for this projects:"
for ((i = 0; i < ${#projects[@]}; i++)); do
  project="${projects[i]}"
  repo=`echo ${project} | awk '{print $1}'`
  echo -n " ${repo}"
done
echo ""
echo "And choose option after that:"
select item in "proceed"; do
  if [[ "$item" = "proceed" ]]; then
    break
  fi
  echo "Add ssh key and choose first option, please:"
done

for ((i = 0; i < ${#projects[@]}; i++)); do
  echo "${project}"
  project="${projects[i]}"
  repo=`echo ${project} | awk '{print $1}'`
  branch=`echo ${project} | awk '{print $3}'`
  git clone ${git}@${host}:${repo} ~/projects/${repo} -b ${branch}
done

